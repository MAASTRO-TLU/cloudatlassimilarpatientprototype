import numpy as np


class SimilarPatientFinder:
    similar_model_features = []

    def __init__(self):
        return

    def parse_similarity_model_features(self, features):
        self.similar_model_features = features
        return

    def top_five_for_reference_patient(self, similarity_model_features_for_ref_patient):
        rank_idx = self.calculate_cumulative_rank(similarity_model_features_for_ref_patient)
        ids = self.rank_patients(rank_idx)
        return ids[0:5]

    def rank_patients(self, rank_idx):
        ranked_patient_array = ["" for x in rank_idx]
        for idx, ref in enumerate(rank_idx):
            ranked_patient_array[idx] = self.similar_model_features[rank_idx[idx]].patient_id
        return ranked_patient_array

    def calculate_cumulative_rank(self, new_patient):
        ptv_rank = self._determine_rank_for_feature(
            [abs(reference_object.ptvVolume - new_patient.ptvVolume)
             for reference_object in self.similar_model_features])
        distance3d_rank = self._determine_rank_for_feature(
            [abs(reference_object.distance3D - new_patient.distance3D)
             for reference_object in self.similar_model_features])
        distance_x_rank = self._determine_rank_for_feature(
            [abs(reference_object.distance[0] - new_patient.distance[0])
             for reference_object in self.similar_model_features])
        distance_y_rank = self._determine_rank_for_feature(
            [abs(reference_object.distance[1] - new_patient.distance[1])
             for reference_object in self.similar_model_features])
        distance_z_rank = self._determine_rank_for_feature(
            [abs(reference_object.distance[2] - new_patient.distance[2])
             for reference_object in self.similar_model_features])
        return np.argsort(ptv_rank + distance3d_rank + distance_x_rank + distance_y_rank + distance_z_rank)

    @staticmethod
    def _determine_rank_for_feature(feature_diff):
        rounded_feature_diff = [int(np.round((x * 10000), 0)) for x in feature_diff]  # rounded float in integer to make comparable
        unique_feature_diff = np.unique(rounded_feature_diff)
        value_rank_map = np.argsort(unique_feature_diff) + 1
        rank = np.empty(len(feature_diff))
        for idx, dummy in enumerate(rank):
            for idx2, value in enumerate(unique_feature_diff):
                if rounded_feature_diff[idx] == value:
                    rank[idx] = value_rank_map[idx2]
        return rank
