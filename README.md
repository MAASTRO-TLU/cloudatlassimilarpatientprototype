# README #

This project was create with PyCharm community version

### What is this repository for? ###

Version 1.0
Proof of concept
Using a reference library select a top 5 of patients with similar anatomy

### How do I get set up? ###

* e-mail me for the reference library
* set the path of ref library in run.py
* run.py

### Who do I talk to? ###

Tim Lustberg (tim.lustberg@maastro.nl)
MAASTRO Clinic