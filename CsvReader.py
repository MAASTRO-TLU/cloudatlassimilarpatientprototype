from RoiPropertiesObject import RoiPropertiesObject
from SimilarityModelFeatures import SimilarityModelFeatures
import csv
import numpy as np


class CsvReader:
    similarityModelFeatures = []
    patientIds = []

    def __init__(self, file_name):
        # read and link rois to a patient
        # ** in this version a single plan per patient is assumed! **
        patient_map = self.__create_patient_map(file_name)
        patient_map = self.__add_rois_to_patient_map(file_name, patient_map)
        self.similarityModelFeatures = self.__store_similarity_model_features(patient_map)

    # def ptv_volume(self):
    #     ptv_volume = np.empty(len(self.patientIds))
    #     for idx, patient_id in enumerate(self.patientIds):
    #         ptv_volume[idx] = self.similarityModelFeatures[patient_id].ptvVolume
    #     return ptv_volume
    #
    # def distance3d(self):
    #     distance3d = np.empty(len(self.patientIds))
    #     for idx, patient_id in enumerate(self.patientIds):
    #         distance3d[idx] = self.similarityModelFeatures[patient_id].distance3D
    #     return distance3d
    #
    # def distance_x(self):
    #     _distance_x = np.empty(len(self.patientIds))
    #     for idx, patient_id in enumerate(self.patientIds):
    #         _distance_x[idx] = self.similarityModelFeatures[patient_id].distance[0]
    #     return _distance_x
    #
    # def distance_y(self):
    #     _distance_y = np.empty(len(self.patientIds))
    #     for idx, patient_id in enumerate(self.patientIds):
    #         _distance_y[idx] = self.similarityModelFeatures[patient_id].distance[1]
    #     return _distance_y
    #
    # def distance_z(self):
    #     _distance_z = np.empty(len(self.patientIds))
    #     for idx, patient_id in enumerate(self.patientIds):
    #         _distance_z[idx] = self.similarityModelFeatures[patient_id].distance[2]
    #     return _distance_z

    @staticmethod
    def __store_similarity_model_features(patient_map):
        similarity_model_features = [SimilarityModelFeatures() for idx in patient_map]  # create feature array
        for idx, patient_id in enumerate(patient_map):
            reference_patient_obj = patient_map[patient_id]
            similarity_model_features[idx].patient_id = patient_id
            similarity_model_features[idx].store_left_lung(reference_patient_obj['lung_l'])
            similarity_model_features[idx].store_right_lung(reference_patient_obj['lung_r'])
            similarity_model_features[idx].store_ptv(reference_patient_obj['ptv'])
            similarity_model_features[idx].calculate_reference_point()
            similarity_model_features[idx].calculate_distance_reference_to_ptv()

        return similarity_model_features

    @staticmethod
    def __create_patient_map(file_name):
        with open(file_name) as csv_file:
            reader = csv.reader(csv_file)
            patient_map = dict()
            for idx, row in enumerate(reader):
                patient_map[row[0]] = dict()
        return patient_map

    @staticmethod
    def __add_rois_to_patient_map(file_name, patient_map):
        with open(file_name) as csvFile:
            reader = csv.reader(csvFile)
            for row in reader:
                new_roi_obj = RoiPropertiesObject(row[3], row[2], row[5], row[7])
                # hardcoded the CSV positions because this is just proof of concept
                roi_objects = patient_map[row[0]]
                roi_objects[row[3].lower()] = new_roi_obj
                patient_map[row[0]] = roi_objects
        return patient_map
