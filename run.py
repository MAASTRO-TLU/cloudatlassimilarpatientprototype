from CsvReader import CsvReader
from SimilarPatientFinder import SimilarPatientFinder

# example script to demonstrate the CloudAtlas SimilarPatientFinder
# the SimilarPatientFinder.py is where the magic happens
# reimplement this proof of concept entirely or update the interfaces
# output can be changed to something that is more useful
#
# INPUT: CSV reference example
# INPUT: PlanReferenceObject to check similarity against the reference database
# OUTPUT: Print the top 5 to the command prompt

# ADD YOUR FILE LOCATION HERE
fileName = 'D:/referencePatientsCONCURRMLDfilteredNoHeader.csv'

# LOAD REFERENCE DATA
csvData = CsvReader(fileName)

# for testing hard code new patient properties (randomly picked from references)
new_patient = csvData.similarityModelFeatures[0]  # 'P0076C0062C0006I1007905'

# DETERMINE SIMILAR PATIENT(S)
similar_patient_finder = SimilarPatientFinder()
similar_patient_finder.parse_similarity_model_features(csvData.similarityModelFeatures)
top5 = similar_patient_finder.top_five_for_reference_patient(new_patient)

for idx, item in enumerate(top5):
    print('rank, patientId')
    print(str(idx+1) + ', ' + item)

# EXAMPLE TO GET INFO ON ANY TOP/RANKING
ranking_idx = similar_patient_finder.calculate_cumulative_rank(new_patient)
top10 = similar_patient_finder.rank_patients(ranking_idx[0:10])

for idx, item in enumerate(top10):
    print('rank, patientId')
    print(str(idx+1) + ', ' + item)
