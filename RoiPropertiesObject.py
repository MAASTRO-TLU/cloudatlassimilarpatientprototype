import re


class RoiPropertiesObject:
    roiName = ''
    plandId = ''
    centerOfMass = []  # in cm
    volume = []  # in CC

    # For the algorithm we need the PTV volume, 3D distance and XYZ distance,
    #  loading the center of mass and volume of the ROIs will ensure we have that info
    #  DVH info is not loaded because we do not need it for the similar patient ranking

    def __init__(self, roi_name, plan_id, volume, center_of_mass):
        self.roiName = roi_name
        self.planId = plan_id
        self.volume = float(volume[1:re.search(',', volume).start()])  # get the first value and convert to float
        self._parse_center_of_mass(center_of_mass)

    def _parse_center_of_mass(self, center_of_mass):
        first_comma_index = re.search(',', center_of_mass).start()
        x = float(center_of_mass[1:first_comma_index])

        center_of_mass = center_of_mass[(first_comma_index + 1):]
        first_comma_index = re.search(',', center_of_mass).start()
        y = float(center_of_mass[1:first_comma_index])

        center_of_mass = center_of_mass[(first_comma_index + 1):]
        z = float(center_of_mass[1:(len(center_of_mass) - 1)])

        self.centerOfMass = [x, y, z]
