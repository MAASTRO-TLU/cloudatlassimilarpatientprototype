import math


class SimilarityModelFeatures:
    patient_id = []
    distance = []
    distance3D = []
    ptvVolume = []

    __reference_point = []
    __left_lung_roi = []
    __right_lung_roi = []
    __ptv_roi = []

    def __init__(self):
        return

    def store_left_lung(self, left_lung):
        self.__left_lung_roi = left_lung

    def store_right_lung(self, right_lung):
        self.__right_lung_roi = right_lung

    def store_ptv(self, ptv):
        self.__ptv_roi = ptv
        self.ptvVolume = ptv.volume

    def store_patient_id(self, patient_id):
        self.patient_id = patient_id

    def calculate_reference_point(self):
        self.__reference_point = [0, 0, 0]
        for idx, centerOfMass in enumerate(self.__left_lung_roi.centerOfMass):
            self.__reference_point[idx] = ((self.__left_lung_roi.centerOfMass[idx] +
                                            self.__right_lung_roi.centerOfMass[idx]) / 2)

    def calculate_distance_reference_to_ptv(self):
        self.distance = [0, 0, 0]
        for idx, ref in enumerate(self.__reference_point):
            self.distance[idx] = self.__ptv_roi.centerOfMass[idx] - ref
        # 3D pythagoras to determine vector length
        self.distance3D = math.sqrt(math.pow(self.distance[0], 2) +
                                    math.pow(self.distance[1], 2) + math.pow(self.distance[2], 2))
